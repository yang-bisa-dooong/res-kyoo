extends KinematicBody2D

export (int) var speed = 400
export (int) var GRAVITY = 1200
export (int) var jump_speed = -400

const UP = Vector2(0,-1)

var velocity = Vector2()

onready var animator = self.get_node("Animator")
onready var sprite = self.get_node("Sprite")

var jump_count= 0

func get_input():
	velocity.x = 0
	if is_on_floor() and Input.is_action_just_pressed('jump'):
		jump_count +=1
		velocity.y = jump_speed
	if !is_on_floor() and Input.is_action_just_pressed('jump') and jump_count==1:
		jump_count = 0
		velocity.y = jump_speed
	if Input.is_action_pressed('ui_right'):
		$Sprite.flip_h = true
		velocity.x += speed
	if Input.is_action_pressed('ui_left'):
		velocity.x -= speed
		$Sprite.flip_h = false

func _process(delta):
	velocity.y += delta * GRAVITY
	get_input()
	velocity = move_and_slide(velocity, UP)

#func _process(delta):
	#pass
	#if velocity.y != 0:
	#	animator.play("Jump")
	#elif velocity.x != 0:
	#	animator.play("Walk")
	#	if velocity.x > 0:
	#		sprite.flip_h = false
	#	else:
	#		sprite.flip_h = true
	#else:
	#	animator.play("Idle")
	